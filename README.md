# Hanami With Parcel

Steps to start:

1. Install dependencies

```
bundle install
yarn install
```

2. Start Parcel

```
yarn dev
```

3. Start Hanami (in separate shell)

```
bundle exec hanami s
```

4. Navigate to http://localhost:2300/ and check JS console for "Hello world" message.